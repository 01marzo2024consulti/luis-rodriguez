package com.example.administracionproductos.Controladores;


import com.example.administracionproductos.DTOS.CredentialsDto;
import com.example.administracionproductos.DTOS.UserDto;
import com.example.administracionproductos.Entidades.Usuarios;
import com.example.administracionproductos.Servicios.Usuarios_Service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/users/")
public class Usuarios_Controler {
    @Autowired
    private Usuarios_Service service;
    @Autowired
    private SecretKey secretKey;

    private final long expirationTime = 864_000_000; // 10 días en milisegundos


    //metodo para listar todos los usuarios
    @GetMapping("/usuarios")
    public List<Usuarios> listarTodosLosUsuarios() {
        return service.listarTodosLosUsuarios();
    }
    //metodo para guardar un usuario

    @PostMapping("/login")
    public ResponseEntity<UserDto> login(@RequestBody CredentialsDto credentialsDto) {
        UserDto userDto = service.login(credentialsDto);

        Date expirationDate = new Date(System.currentTimeMillis() + expirationTime);
        // Agregar los datos del usuario al token JWT
        String token = Jwts.builder()
                .claim("usuarioDTO", userDto)
                .setSubject(credentialsDto.mail()) // Usar el email u otro identificador único
                .setExpiration(expirationDate)
                .signWith(secretKey, SignatureAlgorithm.HS256)
                .compact();
        userDto.setToken(token);
        return ResponseEntity.ok(userDto);
    }






}
