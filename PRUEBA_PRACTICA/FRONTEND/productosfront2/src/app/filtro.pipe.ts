import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {
  transform(value: any[], search: string): any[] {
    if (!value || !Array.isArray(value)) {
      return []; // Si value no es un arreglo, devuelve un arreglo vacío
    }

    if (!search || search.trim() === '') {
      return value; // Si el término de búsqueda está vacío, devuelve todos los elementos sin filtrar
    }

    const searchTerm = search.toLowerCase(); // Convertir el término de búsqueda a minúsculas para hacer la búsqueda insensible a mayúsculas

    return value.filter(item => {
      // Filtrar los elementos según el término de búsqueda en el campo de nombre
      return item.nombre.toLowerCase().includes(searchTerm);
    });
  }
}
