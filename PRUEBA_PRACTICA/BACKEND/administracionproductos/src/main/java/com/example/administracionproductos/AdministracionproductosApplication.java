package com.example.administracionproductos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdministracionproductosApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdministracionproductosApplication.class, args);
	}

}
