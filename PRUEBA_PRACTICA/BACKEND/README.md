Luis Rodriguez
# Backend 
Proyecto creado con spring boot version 3.2
Java 21
Compilador: Maven

## Configuración del Proyecto
1. Clona el repositorio: `git clone [URL del Repositorio]`
2. Crea la Base de datos ejecutando el archivo DB_EXAMEN.sql de la carpeta SCRIPTS
3. Cambia los atributos spring.datasource.username y spring.datasource.password dependiendo de tu configuracion MYSQL en `src/main/resources/application.properties`

## Ejecución del Proyecto

1. Desde la línea de comandos, ejecuta: `mvn spring-boot:run` o desde un IDE con el boton ejecutar
2. El backend estará disponible en `http://localhost:puerto` (por defecto, el puerto es 8080)

## Docker Backend
1. Ejecuta el comando docker compose up


