import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import baseUrl from './helper';
import { BehaviorSubject, Observable, tap } from 'rxjs';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ProductoserviceService {
  constructor(private http: HttpClient) { }

  crearProducto(product:any): Observable<any> {
    // const user = { nombres: nombres, sueldo: sueldo };
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(baseUrl+'/api/productos/save', product, { headers: headers })
        .pipe(tap(res => {
            console.log("crear producto method");
            console.log(res);
        }));
  }
  listarProductos(): Observable<any>{
    return this.http.get(baseUrl+'/api/productos/listar');
  }
  actualizarProducto(id: any, detallesUsuario: any): Observable<any> {
    return this.http.put(baseUrl+'/api/productos/update/'+id, detallesUsuario);
  }
}
