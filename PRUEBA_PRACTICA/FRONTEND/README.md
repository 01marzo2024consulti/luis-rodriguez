Luis Rodriguez
# Frontend
Proyecto generado con [Angular CLI](https://github.com/angular/angular-cli) 

## Requisitos Previos

- [Node.js](https://nodejs.org/) instalado
- [Angular CLI](https://angular.io/cli) instalado

## Configuración del Proyecto

1. Clona el repositorio: `git clone [URL del Repositorio]`
2. Navega al directorio del proyecto angular
3. Ejecuta: `npm install` para instalar las dependencias

## Ejecución del Proyecto

1. Desde la línea de comandos, ejecuta: `ng serve`
2. El frontend estará disponible en `http://localhost:4200`
3. Puedes usar `ng serve -o` y se abrirá tu navegador en la página al finalizar la compilación

## Docker Frontend
1. docker build .
2. docker run -p 4200:80 image_id
