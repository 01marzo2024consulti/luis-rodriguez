import { Component ,  Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.component.html',
  styleUrl: './detalle-producto.component.css'
})
export class DetalleProductoComponent {
  producto: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.producto = data.producto;
  }

}
