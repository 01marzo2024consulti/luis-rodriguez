import java.util.HashMap;
import java.util.Map;
import java.util.Random;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Algoritmo1 {


    public static void main(String[] args) {
        int n_bloques = 1; // Número de bloques de placas a generar
        String provincia = "Guayas"; // Provincia
        String tipo = "Vehículos comerciales"; // Tipo
        System.out.println(generarPlaca(provincia,tipo,n_bloques));


    }

    // Método para generar una placa aleatoria
    private static String generarPlaca(String provincia, String tipo,int n_bloques) {
        Random rand = new Random();
        StringBuilder placas = new StringBuilder();
        StringBuilder placa = new StringBuilder();
        // Generar placas para cada bloque
        for (int i = 0; i < n_bloques; i++) {

            for (int j = 0; j < 3; j++){
                placa.append(obtenerprovinciaabreviada(provincia));
                placa.append(obtenertipoabreviado(tipo));
                // Obtener número entre 3 y 9
                int numero = rand.nextInt(7) + 3;
                placa.append(numero);
                // Agregar guión
                placa.append("-");
                // Obtener números entre 0 y 9 para los últimos 4 caracteres
                for (int k = 0; k < 4; k++) {
                    int digito = rand.nextInt(10);
                    placa.append(digito);
                }
                placa.append("\n");

            }
            placas.append(placa);
            placas.append("\n");
            placa.setLength(0);



        }

        return placas.toString();
    }
    private static String obtenerprovinciaabreviada(String provincia) {
        Map<String, String> PROVINCIAS = new HashMap<>();
        PROVINCIAS.put("Guayas", "G");
        PROVINCIAS.put("Pichincha", "P");
        PROVINCIAS.put("Santa Elena", "Y");
        PROVINCIAS.put("Orellana", "Q");
        PROVINCIAS.put("Carchi", "C");
        return PROVINCIAS.getOrDefault(provincia, "X");

    }
    private static String obtenertipoabreviado(String tipo) {
        Map<String, String> TIPOS = new HashMap<>();
        TIPOS.put("Vehículos comerciales", "AUZ");
        TIPOS.put("Vehículos gubernamentales", "E");
        TIPOS.put("Vehículos de uso oficial", "X");
        TIPOS.put("Vehículo Particular", "B");
        return TIPOS.getOrDefault(tipo, "C");

    }

}


