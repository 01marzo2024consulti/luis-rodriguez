import { Component } from '@angular/core';
import { ProductoserviceService } from '../productoservice.service';
import { MatDialog } from '@angular/material/dialog';
import { DetalleProductoComponent } from '../detalle-producto/detalle-producto.component';
import { MediaService } from '../media.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrl: './productos.component.css'
})
export class ProductosComponent {
  data = [];
  imagenglobal:any;
  search:any;
  columnas = ['nombre', 'valor', 'fechaIngreso', 'fechaVenta', 'estado',  'acciones'];

  
  constructor(private productoService: ProductoserviceService,private dialog: MatDialog,private mediaservice: MediaService) {

    
   }

  ngOnInit() {
    this.obtenerDatos();
  }

  obtenerDatos() {
    this.productoService.listarProductos().subscribe(
      response => {
        this.data = response;
      },
      error => {
        console.error('Error al obtener datos:', error);
      }
    );
  }
  uploadImage(event:any,objecproduct: any){
    const file=event.target.files[0];
    const formData = {
      nombre:objecproduct.nombre,
    descripcion:objecproduct.descripcion,
   valor:objecproduct.valor,
    marca:objecproduct.marca,
     
   };
   console.log("id:");
   
   console.log(objecproduct.producto_id);
   console.log(formData);
   
    if(file){
      const formDatos=new FormData();
    formDatos.append('file',file)
     this.mediaservice.uploadFile(formDatos).subscribe((response: any) => {
     // const urlSinPrefijo = response.url.replace("http://localhost:8080/media/", "");
      //const rutaLocal = decodeURIComponent(response.url);

      const datosCompletos = {
        ...formData,
        fecha_creacion: new Date(),
       fecha_venta:"N/A",
       imagen:response.url,
       estado:"Activo"
      };

      console.log('Datos enviados correctamente:', response.url);
      console.log(datosCompletos);
      this.productoService.actualizarProducto(objecproduct.producto_id,datosCompletos).subscribe((response: any) => {
        alert('Imagen subida correctamente');
        this.obtenerDatos();
  
       
       
      },
      (error) => {
        
        console.error('Error al guardar la imagen:', error);
      });


      
     
    },
    (error) => {
      alert('Error en el envio');
      console.error('Error al enviar datos:', error);
    });
    

    }
    


      

    }

    borrar(objecproduct: any){
      
      const formData = {
        nombre:objecproduct.nombre,
      descripcion:objecproduct.descripcion,
     valor:objecproduct.valor,
      marca:objecproduct.marca,
       
     };
     const datosCompletos = {
      ...formData,
      fecha_creacion:objecproduct.fecha_creacion,
     fecha_venta:objecproduct.fecha_venta,
     imagen:objecproduct.imagen,
     estado:"Desactivado"
    };

   
    this.productoService.actualizarProducto(objecproduct.producto_id,datosCompletos).subscribe((response: any) => {
      alert('Cambio de estado');
      this.obtenerDatos();

     
     
    },
    (error) => {
      
      console.error('Error al guardar la imagen:', error);
    });
     
     
     
    
      
  
      
      
  
  
        
  
      }


  verDetalle(row: any) {
    this.dialog.open(DetalleProductoComponent, {
      data: { producto: row }
    });
  }

  buscarnombre(search:string){
    this.search=search;
    console.log(search);

  }



}
