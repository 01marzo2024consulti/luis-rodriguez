package com.example.administracionproductos.Servicios;

import com.example.administracionproductos.Entidades.Productos;
import com.example.administracionproductos.Repositorios.ProductosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductosService {
    @Autowired
    private ProductosRepository repository;
    public List<Productos> listarTodos() {
        return repository.findAll();
    }
    public Productos guardar(Productos producto) {
        return repository.save(producto);
    }

    public Productos obtenerProductoPorId(Long id){
        return repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("No existe entidad con el ID : " + id));
    }
    public Productos actualizarProducto(Long id,Productos detallesProducto){
        Productos product = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("No existe usuario con el ID : " + id));


// Establecer los valores de los atributos

        product.setNombre(detallesProducto.getNombre());
        product.setDescripcion(detallesProducto.getDescripcion());
        product.setValor(detallesProducto.getValor());
        product.setMarca(detallesProducto.getMarca());
        product.setFecha_creacion(detallesProducto.getFecha_creacion());
        product.setFecha_venta(detallesProducto.getFecha_venta());
        product.setImagen(detallesProducto.getImagen());
        product.setEstado(detallesProducto.getEstado());






        return repository.save(product);
    }
}
