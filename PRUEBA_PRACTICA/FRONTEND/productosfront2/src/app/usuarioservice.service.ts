import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import baseUrl from './helper';
import { CookieService } from 'ngx-cookie-service';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class UsuarioserviceService implements CanActivate{

  constructor(private http: HttpClient, private router: Router,private cookieService: CookieService) { }
  
  usuarioactual:any;
  canActivate(): boolean {
    if (this.isLoggedIn()) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
  
  login(email: string, password: string): Observable<any> {
    const user = {
      mail: email,
      password: password
    };
     this.usuarioactual=this.http.post(
      baseUrl + '/api/users/login',
      user,
      httpOptions
    ).pipe(tap(res => {
      if (res && (res as any).token) {
        //this.changeUser(res); // Actualiza los datos del usuario

       sessionStorage.setItem('token', (res as any).token);
      }
    }));
    return this.usuarioactual;
    
    
  }

  logout(): void {
    // Elimina el token del almacenamiento local para cerrar la sesión del usuario
    sessionStorage.removeItem('token');
    
    //this.changeUser(null);
  }

  isLoggedIn(): boolean {
    return sessionStorage.getItem('token') !== null;
  }
  listarUsuarios(): Observable<any>{
    return this.http.get(baseUrl+'/api/users/usuarios');
  }
}

