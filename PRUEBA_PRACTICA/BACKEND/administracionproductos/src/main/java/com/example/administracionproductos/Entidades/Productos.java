package com.example.administracionproductos.Entidades;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "productos")
public class Productos {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long producto_id;

    public String nombre;
    public String descripcion;
    public Float valor;
    public String marca;
    public String fecha_creacion;
    public String fecha_venta;
    public String imagen;
    public String estado;


}
