import { Component } from '@angular/core';
import { ProductoserviceService } from '../productoservice.service';

@Component({
  selector: 'app-crearproductos',
  templateUrl: './crearproductos.component.html',
  styleUrl: './crearproductos.component.css'
})
export class CrearproductosComponent {
  formData: any = {};
  constructor(private productoService: ProductoserviceService) { }


  submitForm() {
    

   
    const formData = {
       nombre:this.formData.nombre,
     descripcion:this.formData.descripcion,
    valor:this.formData.valor,
     marca:this.formData.marca,
    
     
    };

    console.log(formData);
    const datosCompletos = {
      ...formData,
      fecha_creacion: new Date(),
     fecha_venta:"N/A",
     imagen:"N/A",
     estado:"Activo"
    };


    
    
    this.productoService.crearProducto(datosCompletos).subscribe(
        (response) => {
          alert('Datos enviados correctamente');
          console.log('Datos enviados correctamente:', response);
          
         
        },
        (error) => {
          alert('Error en el envio');
          console.error('Error al enviar datos:', error);
        }
      );
    
    
  }
  limpiarFormulario() {
    this.formData = {
      nombre: '',
      descripcion: '',
      valor: '',
      marca: ''
    };
    
  }
}
