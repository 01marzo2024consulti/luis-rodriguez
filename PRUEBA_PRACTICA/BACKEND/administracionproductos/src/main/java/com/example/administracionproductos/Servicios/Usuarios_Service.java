package com.example.administracionproductos.Servicios;

import com.example.administracionproductos.DTOS.CredentialsDto;
import com.example.administracionproductos.DTOS.UserDto;
import com.example.administracionproductos.Entidades.Usuarios;
import com.example.administracionproductos.Exception.AppException;
import com.example.administracionproductos.Mappers.UserMapper;
import com.example.administracionproductos.Repositorios.Usuarios_Repository;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@RequiredArgsConstructor
@Service
public class Usuarios_Service {
    @Autowired
    private Usuarios_Repository repository;
    private final UserMapper userMapper;



    public List<Usuarios> listarTodosLosUsuarios() {
        return repository.findAll();
    }







    public UserDto login(CredentialsDto credentialsDto) {
        System.out.println(credentialsDto.mail());
        Usuarios user = repository.findByMail(credentialsDto.mail())
                .orElseThrow(() -> new AppException("Unknown user", HttpStatus.NOT_FOUND));
        if (user.getContraseña().equals(new String(credentialsDto.password()))) {
            UserDto userDto = userMapper.toUserDto(user);
            userDto.setUsuario_id(user.getUsuario_id());
            userDto.setMail(user.getMail());
            userDto.setEstado(user.getEstado());


            return userDto;
        }
        throw new AppException("Invalid password", HttpStatus.BAD_REQUEST);
    }














}
