import { Component } from '@angular/core';
import { UsuarioserviceService } from '../usuarioservice.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  login: string = "";
  userPassword: string = "";
  isSuccessful = false;
  isSignInFailed = false;
  errorMessage = '';


  constructor(private userService:UsuarioserviceService, private router: Router,private _snackBar: MatSnackBar){}
  onSubmitLogin(): void {
    this.userService.login(this.login, this.userPassword).subscribe(
      (respuesta) => {
        console.log(respuesta);
        console.log(respuesta.rolUsuario);
        sessionStorage.setItem('token', respuesta.token);
       
        
        console.log(sessionStorage.getItem('token'));
        
       
       
        
        this.isSuccessful=true;
        
          this.router.navigate(['/productos']);
        

       
        
      },
      (error) => {
        this.errorMessage = 'Credenciales incorrectas';
        this.isSignInFailed=true;
        console.log(error);
        this.mostrarNotificacion(this.errorMessage);
      }
    );
  }
 
  mostrarNotificacion(mensaje: string): void {
    this._snackBar.open(mensaje, 'Cerrar', {
      duration: 3000 // Duración de la notificación en milisegundos
    });
  }
}
