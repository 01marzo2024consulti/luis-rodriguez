package com.example.administracionproductos.Repositorios;



import com.example.administracionproductos.Entidades.Usuarios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface Usuarios_Repository extends JpaRepository<Usuarios, Long> {

    Optional<Usuarios> findByMail(String mail);


}
