package com.example.administracionproductos.Servicios;


import com.example.administracionproductos.Repositorios.GuardarImagenRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class GuardarImagenService implements GuardarImagenRepository {
    @Value("${media.location}")
    private  String medialocation;
    private Path rootlocation;
    @Override
    @PostConstruct
    public void init() throws IOException {
        rootlocation=Paths.get(medialocation);
        Files.createDirectories(rootlocation);


    }

    @Override
    public String store(MultipartFile file) {
        try {
            if (file.isEmpty()) {
                throw new RuntimeException("Archivo vacío");
            }

            String filename = file.getOriginalFilename();
            String savedFileName = UUID.randomUUID().toString() + "_" + filename; // Genera un nombre único para el archivo guardado

            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, rootlocation.resolve(savedFileName), StandardCopyOption.REPLACE_EXISTING);
            }

            // Devuelve solo el nombre del archivo guardado
            return savedFileName;

        } catch (IOException e) {
            throw new RuntimeException("Falla al guardar el archivo", e);
        }
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file=rootlocation.resolve(filename);
            Resource resource= new UrlResource((file.toUri()));

            if (resource.exists() || resource.isReadable()){
                return  resource;
            }else{
                throw  new RuntimeException("Archivo no leido:" + filename);
            }

        }catch (MalformedURLException e){
            throw  new RuntimeException( "No se puede lee el archivo "+ filename);
        }

    }
}
