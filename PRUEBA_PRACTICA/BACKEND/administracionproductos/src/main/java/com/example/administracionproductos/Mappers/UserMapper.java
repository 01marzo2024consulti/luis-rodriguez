package com.example.administracionproductos.Mappers;

import com.example.administracionproductos.DTOS.UserDto;
import com.example.administracionproductos.Entidades.Usuarios;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto toUserDto(Usuarios user);



}
