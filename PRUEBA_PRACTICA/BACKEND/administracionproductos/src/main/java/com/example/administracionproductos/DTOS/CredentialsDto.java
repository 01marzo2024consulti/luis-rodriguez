package com.example.administracionproductos.DTOS;

public record CredentialsDto(String mail, char[] password) { }
