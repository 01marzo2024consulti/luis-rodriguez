package com.example.administracionproductos.Controladores;

import com.example.administracionproductos.Entidades.Productos;
import com.example.administracionproductos.Servicios.ProductosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/productos/")
public class ProductosController {

    @Autowired
    private ProductosService service;


    //metodo para listar todos los elementos
    @GetMapping("/listar")
    public List<Productos> listarTodos() {
        return service.listarTodos();
    }
    //metodo para guardar
    @PostMapping("/save")

    public Productos guardar(@RequestBody Productos producto) {
        return service.guardar(producto);
    }

    //metodo para buscar por id

    @GetMapping("/{id}")
    public ResponseEntity<Productos> obtenerPorId(@PathVariable Long id){
        return ResponseEntity.ok(service.obtenerProductoPorId(id));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Productos> actualizarProducto(@PathVariable Long id, @RequestBody Productos detallesProductos) {
        return ResponseEntity.ok(service.actualizarProducto(id, detallesProductos));
    }



}
