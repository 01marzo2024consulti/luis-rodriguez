package com.example.administracionproductos.DTOS;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {

    public Long usuario_id;
    public String mail;
    private String token;
    public String estado;



    @Override
    public String toString(){
        return usuario_id+" "+mail+" "+token+" "+estado;
    }
}
