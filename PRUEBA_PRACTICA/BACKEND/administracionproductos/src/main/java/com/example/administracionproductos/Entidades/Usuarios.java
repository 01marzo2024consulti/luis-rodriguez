package com.example.administracionproductos.Entidades;

import jakarta.persistence.*;
import lombok.Data;


@Data
@Entity
@Table(name = "usuarios")
public class Usuarios {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long usuario_id;
    public String mail;
    public String contraseña;
    public String estado;



    // Constructor vacío y constructor con parámetros
    public Usuarios() {}

    public Usuarios(Long id) {
        this.usuario_id = id;
    }





}
