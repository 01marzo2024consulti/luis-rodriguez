import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import baseUrl from './helper';

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  constructor(private http: HttpClient, private router: Router) { }
  uploadFile(formData:FormData
    ): Observable<any>{
    return this.http.post(baseUrl+'/api/media/upload',formData);
  }
  



  
}
