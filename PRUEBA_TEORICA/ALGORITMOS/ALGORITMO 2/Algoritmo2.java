//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Algoritmo2 {
    public static void main(String[] args) {
        String cadena = "casarotal";
        try {
            System.out.println(ComprobarCadena(cadena));

        }catch (Exception e){
            System.out.println(e.getMessage());
        }



    }
    public static String ComprobarCadena(String cadena) throws Exception{
        int longitud = cadena.length();

        if (longitud % 2 == 0) {
            // Si la longitud es par
            String primeraParte = cadena.substring(0, longitud / 2);
            String segundaParte = cadena.substring(longitud / 2);
            return segundaParte + primeraParte;
        } else {
           throw new Exception("La cadena ingresada tiene una longitud impar:"+ longitud);

        }


    }

}