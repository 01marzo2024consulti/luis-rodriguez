package com.example.administracionproductos.Controladores;


import com.example.administracionproductos.Servicios.GuardarImagenService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/api/media/")
@AllArgsConstructor

public class GuardarImagenController {
    private  final GuardarImagenService service;
    private  final HttpServletRequest request;

    @GetMapping("{filename:.+}")
    public ResponseEntity <Resource> getFile(@PathVariable String filename) throws IOException {
        Resource file= service.loadAsResource(filename);
        String contentType= Files.probeContentType(file.getFile().toPath());

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_TYPE, contentType)
                .body(file);
    }

    @PostMapping("upload")
    public Map<String,String> uploadFile(@RequestParam("file") MultipartFile multipartFile) {
        String path=service.store(multipartFile);
        String host = request.getRequestURL().toString().replace(request.getRequestURI(),"");
        String url= ServletUriComponentsBuilder
                .fromHttpUrl(host)
                .path("api/media/")
                .path(path)
                .toUriString();



        return Map.of("url",url);

    }


}
