package com.example.administracionproductos.Configuracion;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfig {
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
//                System.out.println("Permitiendo el acceso a todo origin no credentials allowed methods");
                registry.addMapping("/**")
                        //url default de proyecto de angular
                        .allowedOrigins("http://localhost:4200/")  // Reemplaza con la URL de tu aplicación Angular
                        //url para proyecto de angular dockerizado
//                        .allowedOrigins("http://localhost:8888/")  // Reemplaza con la URL de tu aplicación Angular
                        .allowedMethods("GET", "POST", "PUT", "DELETE")
                        .allowCredentials(true);
            }
        };
    }
}

